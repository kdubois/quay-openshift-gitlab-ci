import requests
import time
from collections import Counter
import sys
import argparse

__author__ = 'Kevin Dubois'
__version__ = '1.0.0'

"""scanresult.py: Examine the results of a container image scan in Quay and exit with an error if the severity is too high"""

# Arguments
parser = argparse.ArgumentParser(
    description="""Examine the results of a container image scan in Quay and exit with an error if the severity is too high.""")

parser.add_argument('-a', '--app', required=True,
                    help="""application/repository name""")
parser.add_argument('-n', '--namespace', required=True,
                    help="""registry namespace""")
parser.add_argument('-r', '--registry', default='quay.io',
                    help="""registry name (eg. quay.io)""")
parser.add_argument('-c', '--creds', required=True, metavar="credentials",
                    help="""registry credentials, formatted as username:password""")
parser.add_argument('-t', '--tag', default='latest', help="""image tag""")
args = parser.parse_args()

APP = args.app
REGISTRY_NAMESPACE = args.namespace
REGISTRY = args.registry
REG_CREDS = args.creds
TAG = args.tag

# Threshold to fail the build.
threshold_list = ('Critical', 'High')
timeout_minutes = 10

QUAY_API = "https://{}/api/v1/repository/{}/{}".format(
    REGISTRY, REGISTRY_NAMESPACE, APP)

images_url = '{}/tag'.format(QUAY_API)

# get image id of first (most recent) result.
image_result = ""
try: 
    print("images url was: {0}".format(images_url))
    image_result = requests.get(images_url).json()
    # get the first item from the list returned (it should be the latest)
    manifest = image_result["tags"][0]["manifest_digest"]
except RuntimeError as err:
    print("Runtime Error: {0}".format(err))


timeout = time.time() + 60 * timeout_minutes

# retrieve the packages installed in this image and extract the number of different vulnerabilities
while True:
    if (time.time() > timeout):
        print("Timed out after {} minutes.".format(timeout_minutes))
        exit(1)

    security_url = '{}/manifest/{}/security?vulnerabilities=true'.format(
        QUAY_API, manifest)
    vuln_result = requests.get(security_url).json()

    if vuln_result["status"] != 'scanned':
        time.sleep(5)
        continue

    # count the number of vulnerabilities per severity level
    c = Counter(v['Severity'] for vs in vuln_result['data']
                ['Layer']['Features'] for v in vs['Vulnerabilities'])
    break

vulnerabilities = c.most_common(10)

# Order vulnerabilities by priority
priorities = ('Critical', 'High', 'Medium', 'Low', 'Negligible', 'Unknown')
ordered_vulnerabilities = sorted(
    vulnerabilities, key=lambda x: priorities.index(x[0]))

# Print results, and if the threshold has been met, fail the build
print('---------------- QUAY CLAIR SCAN RESULTS ----------------')
for vulnerability in ordered_vulnerabilities:
    print('Image Has {} {} Vulnerabilities'.format(
        vulnerability[1], vulnerability[0]))

if ([v[0] for v in vulnerabilities if threshold_list.count(v[0])]):
    print("\nImage did not meet acceptable threshold, marking NOT ACCEPTABLE")
    exit(1)
